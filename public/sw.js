importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.1.3/workbox-sw.js')


workbox.precaching.precacheAndRoute([
  {url: '/index.html', revision: null },
  {url: '/js/App.js', revision: null},
  {url: '/js/TodoCreator.js', revision: null},
  {url: '/js/TodoList.js', revision: null},
])