import { html, render, Component } from 'https://unpkg.com/htm/preact/standalone.module.js'
import TodoList from './TodoList.js';
import TodoCreator from './TodoCreator.js';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: this.get_todos()
        };
    }

    render() {
        return html`
            <div class="container">
                <h3>TODO APP</h3>
                <${TodoCreator} add=${this.add.bind(this)}/>
                <${TodoList} list=${this.state.todos} remove=${this.remove.bind(this)} update=${this.update.bind(this)}/>
            </div>`;
    }

    remove(id) {
        var new_todos = this.state.todos;
        new_todos.splice(id, 1);
        this.update_todos(new_todos);
    }

    update(id, newValue) {
        newValue = newValue.trim();
        if (newValue !== '') {
            var new_todos = this.state.todos;
            new_todos[id] = newValue;
            this.update_todos(new_todos);
        }
    }

    add(todo) {
        todo = todo.trim();
        if (todo !== '') {
            var new_todos = this.state.todos;
            new_todos.push(todo);
            this.update_todos(new_todos);
        }
    }

    get_todos() {
        var todos_str = localStorage.getItem('todo');
        if (todos_str !== null)
            return JSON.parse(todos_str);
        return [];
    }

    update_todos(new_todos) {
        localStorage.setItem('todo', JSON.stringify(new_todos));
        this.setState({todos: new_todos});
    }
}

render(html`<${App}/>`, document.body);
