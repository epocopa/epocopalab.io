import { html, render, Component } from 'https://unpkg.com/htm/preact/standalone.module.js'

class TodoList extends Component {
    constructor(props) {
        super(props);
        this.list = this.props.list;
        this.newValue = '';
        this.state = {
            editId: -1
        };
    }

    render() {
        return html`
            <ul>
                ${this.list.map((todo, id) => html`
                    ${id === this.state.editId ?
                        html`
                        <li><input typed="text" id="${id}" value="${todo}" onchange="${this.handleTodoChange.bind(this)}"></input></li>
                        <button onclick=${this.handleRemove.bind(this, id)}>Delete</button>
                        <button onclick=${() => this.setState({editId: -1})}>Cancel</button>
                        <button onclick=${this.handleUpdate.bind(this, id)}>Save</button>`
                    :
                        html`<li><button id="${id}" onclick="${() => this.set_edit(id)}">${todo}</button></li>`
                    }
                `)}
            </ul>`;
    }

    set_edit(id) {
        this.setState({editId: id});
    }

    handleTodoChange(e) {
        var newVal = e.target.value;
        this.newValue = newVal;
    }

    handleRemove(id) {
        this.props.remove(id);
        this.setState({editId: -1});
    }

    handleUpdate(id) {
        this.props.update(id, this.newValue);
        this.setState({editId: -1});
    }
}

export default TodoList;